# SAMPLE PROJECTS

# matlab_CreditRisk_sample
| In collaboration with University assistant at Faculty of Organizational Sciences, analyzing portfolio of corporate obligors and simulate possible outcomes (gains and losses of a portfolio, calculateing Value-at-Risk and Expected Shortfall).
| DevEnv: Matlab -v 2013a
| I don't own datasets used for this project, and I'm sending portion of codes that cover following topics of interest:

  Why this project?
  Matlab, correlations, credit ratings, Cholesky factorization 


# python_MachineLearning_script
| Private project, working on alternative ways of building credit scoring system for corporate sector companies in Serbia and Germany based on Business Registers Agencies data.
| DevEnv: Python 3.5
| I don't own dataset used, it is provided by a company that I currently work in, but in order to test it I can provide some mock data if necessary.

  Why this project?
  Missing values, [sklearn, panda, numpy], categorical variables, machine learning, performance measures
  

# python_TwitterAPIStats_project
| Cooperation with finTech company in Belgrade to analyze tweets that contain certain words related to trading.
| DevEnv: Python 2.7
| I own this project and I fully share it.

  Why this project?
  Twitter API, json, sentiment analysis, regEx
  
  Folder structure is far from optimal - u live u learn. :)
  That's why I'm sending additional explanations:
 
  @ AFINN-111.txt
    - Source of word sentiment (-4 most negative, +4 post positive sentiment)
	- I do not own this file, provided by instructors during some Coursera online course.
  @ search_results.json
    - Twitter search sample data
  @ twitter_search.py
  	- Using Twitter API to fetch sample of tweets that contain words of interest and saves them to .txt file (eg. search_results_1.json)
  @ chart.html
    - Simple, simple way of plotting word frequency trough time
  @ twitter_analyze.py
    - Fetch sentiments and tweets from file and return dictionary
	- Remove stopwords, punctuation and symbols
	- For each word (set in setup region), get date when tweet is created
    - Assingn scores to tweets (based on sentiment file). Find words without score - calculate and assign score to every new word and save to new_sentiment.txt. (eg. Bitcoin got -1 sentiment value)
    - Find out where from most positive tweets are coming from
    - Find and count specific words, like hashtags, urls, numbers etc. (to cutomize change RegEx)
	- Get data about connections among users



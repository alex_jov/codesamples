import os
import pandas as pd
from sklearn import tree
from sklearn.cross_validation import train_test_split
from sklearn import preprocessing
from sklearn.metrics import confusion_matrix
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
from sklearn import linear_model

dir = os.path.dirname(os.path.realpath(__file__))

data_path = os.path.join(dir, "Data")

'''
Python 3.5, Conda 3 Env

Analysies history of corporate sector financial ratios to model default (binary variable indicates failure to meet the legal obligation of debt repayment)
'''

if __name__ == '__main__':

    print("Start")

    # Import
    corporate_data_file_path = os.path.join(data_path, "CorporateData_21000.csv")

    with open(corporate_data_file_path) as file:

        sample = pd.read_csv(file, index_col=0, parse_dates=True)

    # Info
    print(sample.head())
    print(sample.info())

    # Missing values
    for col in sample:
        if sample[col].isnull().sum()/len(sample) > 0.2:
            sample = sample.drop(col, 1)

    sample = sample.dropna()

    sample = sample.reset_index(drop=False)

    print(sample.info())

    # Desc
    print("[sample] Percent of defaulters: " + str(len(sample[sample.DefaultIndicator == 1]) / len(sample) * 100))
    print("[sample] Percent of non-defaulters: " + str(len(sample[sample.DefaultIndicator == 0]) / len(sample) * 100))

    '''for col in sample:
        print(col + str(sample[col].value_counts))'''

    # 10k rule
    classes, y_indices, y_count = np.unique(sample.SectorCode, return_inverse=True, return_counts=True)

    small_bins = classes[y_count <= 10]

    sample = sample[~sample.SectorCode.isin(small_bins)]

    sample = sample.reset_index(drop=False)

    # Categorical variables - workaround
    for col in sample:
        if str(sample[col].values.dtype) in 'object':
            le = preprocessing.LabelEncoder()
            res = le.fit(sample[col].values)

            sample[col] = le.transform(sample[col].values)

    # Split
    # Note: seed
    train, validate = train_test_split(sample, random_state=0, test_size=0.3, stratify=sample.SectorCode)

    print("[train sample] Percent of defaulters: " + str(len(train[train.DefaultIndicator == 1])/len(train)*100))
    print("[train sample] Percent of non-defaulters: " + str(len(train[train.DefaultIndicator == 0])/len(train)*100))

    print("[validation sample] Percent of defaulters: " + str(len(validate[validate.DefaultIndicator == 1])/len(validate)*100))
    print("[validation sample] Percent of non-defaulters: " + str(len(validate[validate.DefaultIndicator == 0])/len(validate)*100))

    # Predictors
    predictor_columns = list(train)

    remove_values = ["DefaultIndicator", "ID", "index"] # TODO: ne sme zakucano

    predictor_columns = [i for j, i in enumerate(predictor_columns) if i not in remove_values]

    '''
    Decision Trees (DTs) are a non-parametric supervised learning method used for classification and regression
    '''

    # 1. Tree CLASSIFIER
    # Note: seed

    dt_classifier = tree.DecisionTreeClassifier(random_state=0)

    dt_classifier = dt_classifier.fit(train[predictor_columns], train.DefaultIndicator)

    y_predict = dt_classifier.predict(validate[predictor_columns])

    # 2. Tree REGRESSOR
    '''
    clf_regressor = tree.DecisionTreeRegressor()

    clf_regressor = clf_regressor.fit(train[predictor_columns], train.DefaultIndicator)

    y_predict = clf_regressor.predict(validate[predictor_columns])
    '''
    # Confusion matrix
    y_true = validate.DefaultIndicator

    # Compute confusion matrix values
    cm = confusion_matrix(y_true, y_predict)
    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    print('Confusion matrix, without normalization')

    print(pd.crosstab(y_true, y_predict, rownames=['True'], colnames=['Predicted'], margins=True))

    print('Normalized confusion matrix')
    print(cm_normalized)

    # Performance measures
    d = dict()

    # (True positive + true negative) / total population
    d["accuracy"] = (cm[0, 0] + cm[1, 1])/len(y_true)

    # Hit / (hit + miss)
    d["hit_rate"] = cm[1, 1]/(cm[1, 1] + cm[1, 0])

    # false alarm / (false alarm + true)
    d["false_alarm_rate"] = cm[0, 1]/(cm[0, 1] + cm[0, 0])

    performance = pd.Series(d)

    print(performance)

    # TODO worse performance on larger samples - overfit or coincidence?

    # Compute ROC curve and ROC area for each class
    # Note: ROC curve plots HR against FAR (same measure)
    roc_auc = dict()

    fpr, tpr, _ = roc_curve(y_true, y_predict)
    roc_auc = auc(fpr, tpr)

    # Plot of a ROC curve for a specific class
    plt.figure()
    plt.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show(block=False)

    # You need to calculate Gini
    # To do that calculate PD instead of indicator variable so you can get percent of defaulted companies

    # Logit
    X = train[predictor_columns]
    y = train.DefaultIndicator

    X_test = validate[predictor_columns]

    # run the classifier
    logreg = linear_model.LogisticRegression(C=1e5, random_state=0)
    logreg.fit(X, y)

    y_predict = logreg.predict(validate[predictor_columns])

    # print(pd.DataFrame(zip(X.columns, np.transpose(logreg.coef_))))

    cm = confusion_matrix(y_true, y_predict)

     # Performance measures
    d = dict()

    # (True positive + true negative) / total population
    d["accuracy"] = (cm[0, 0] + cm[1, 1])/len(y_true)

    # Hit / (hit + miss)
    d["hit_rate"] = cm[1, 1]/(cm[1, 1] + cm[1, 0])

    # false alarm / (false alarm + true)
    d["false_alarm_rate"] = cm[0, 1]/(cm[0, 1] + cm[0, 0])

    print(pd.Series(d))

    print("END")


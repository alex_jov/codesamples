import sys
import os
import json
import operator
import re
from nltk.corpus import stopwords
import string
import vincent
import count_terms
import pandas
import glob
import twitter_search

#import networkx as net
#import matplotlib.pyplot as plt

from collections import defaultdict
import math

'''
ENV:
Python 2.7

Desc:
Performs analysis on existing tweets data
'''

dir = os.path.dirname(__file__)

sentiment_file_path = os.path.join(dir, 'AFINN-111.txt')
tweet_file_path = os.path.join(dir, 'search_results_1.json')
new_sentiments_path = os.path.join(dir, 'new_sentiments.txt')
followers_list_file_path = os.path.join(dir, 'followers_list.json')

'''
Fetch sentiments from file and return dictionary
'''


def get_scores():
    try:
        # tab delimited txt with terms and scores to dictionary
        scores = {}
        with open(sentiment_file_path) as file:
            for line in file:
                term, score = line.split("\t")
                scores[term] = int(score)

        return scores

    except (OSError, IOError):
        print("[ERR] File with sentiments not found at location: " + sentiment_file_path)
        sys.exit(1)


'''
Fetch tweets from file
'''


def get_tweets():
    try:
        # parse tweets
        with open(tweet_file_path) as file:
            tweets = json.load(file, encoding='utf-8')

        if not tweets:
            print("No tweets found :(")
            sys.exit()

        return tweets

    except (OSError, IOError):
        print("[ERR] File with tweets not found at location: " + tweet_file_path)
        sys.exit(1)


'''
Find words without score
Based on tweets in tweet file, assign them scores
'''


def assign_new_scores(tweets, scores):
    words_without_score = {}

    for tweet in tweets:
        if 'text' in tweet:

            tweet_score = 0
            words = tweet['text'].split(" ")

            for word in words:
                if word in scores:
                    tweet_score += scores[word]
                else:
                    if (len(word) > 2 and 'http' not in word):  # ignore links and words shorter than 3 letters
                        if word not in words_without_score:
                            words_without_score[word] = []  # add to new words

            tweet["score"] = tweet_score

            for word in words:
                if word in words_without_score:
                    words_without_score[word].append(tweet_score)

    words_with_score = {}
    for word in words_without_score:
        words_with_score[word] = sum(words_without_score[word]) / len(words_without_score[word])  # avg

    # jic: merge dictionaries with scores
    all_scores = scores.copy()
    all_scores.update(words_with_score)

    # save (only) new words to the file
    with open(new_sentiments_path, "w+") as my_file:
        for score in words_with_score:
            my_file.write(str(score.encode('utf-8')).replace("\n", "") + "\t" + str(words_with_score[score]) + "\n")


'''
Find out where from most positive tweets are coming from
'''


def determine_location_of_tweets(tweets):
    location_score = {}
    for tweet in tweets:
        if 'text' in tweet:

            if 'user' in tweet:  # The user who posted this Tweet
                if tweet['user'] is not None:
                    if 'location' in tweet['user']:

                        location = tweet['user']['location']

                        if not location:
                            # the tweet is associated (but not necessarily originating from) a Place
                            if 'place' in tweet:
                                if tweet['place']:
                                    if 'country_code' in tweet['place']:
                                        location = tweet['place']['country_code']

            if not location:
                continue

            if not re.match("[a-zA-Z0-9-, .]*$", location) or 'follow' in location.lower():
                print("[INFO] Location skipped: " + location.encode('utf-8'))
                continue

            print(str(tweet['id']) + ", " + str(location.encode('utf-8')) + ", score: " + str(tweet["score"]))

            # unique pairs
            location_score[location] = location_score.get(location, 0) + tweet["score"]  # TODO make relative

    # find the one with the happiest tweets
    most_accepted = max(location_score.items(), key=operator.itemgetter(1))[0]
    print("[INFO] Best scores in country: " + most_accepted)


'''
Find and count specific words, like hashtags, urls, numbers etc.
'''


def analyze_tweets(tweets):
    html_tags = [r'<[^>]+>']

    mentions = [r'(?:@[\w_]+)']

    hash_tags = [r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)"]

    urls = [r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+']

    numbers = [r'(?:(?:\d+,?)+(?:\.?\d+)?)']

    html_tags_re = re.compile(r'^'.join(html_tags) + '$', re.VERBOSE | re.IGNORECASE)
    mentions_re = re.compile(r'^'.join(mentions) + '$', re.VERBOSE | re.IGNORECASE)
    hash_tags_re = re.compile(r'^'.join(hash_tags) + '$', re.VERBOSE | re.IGNORECASE)
    urls_re = re.compile(r'^'.join(urls) + '$', re.VERBOSE | re.IGNORECASE)
    numbers_re = re.compile(r'^'.join(numbers) + '$', re.VERBOSE | re.IGNORECASE)

    all_html = all_mentions = all_hash_tags = all_urls = all_numbers = []

    for tweet in tweets:
        all_html.append(True if html_tags_re.search(tweet['text']) is not None else False)  # TODO Python indexing

        all_mentions.append(True if mentions_re.search(tweet['text']) is not None else False)

        all_hash_tags.append(True if hash_tags_re.search(tweet['text']) is not None else False)

        all_urls.append(True if urls_re.search(tweet['text']) is not None else False)

        all_numbers.append(True if numbers_re.search(tweet['text']) is not None else False)

    sum_html = sum(all_html)
    sum_mentions = sum(all_mentions)
    sum_hash_tags = sum(all_hash_tags)
    sum_urls = sum(all_urls)
    sum_numbers = sum(all_numbers)

    if sum_html == sum_mentions == sum_hash_tags == sum_urls == sum_numbers:
        print("[Warning] function, why u no work :( :'( ")
        return

    print('[INFO] HTML :{0} \nMentions: {1} \nHash-tags: {2} \nURLs: {3} \nNumbers: {4}'.format(sum_html, sum_mentions,
                                                                                                sum_hash_tags, sum_urls,
                                                                                                sum_numbers))


'''
Remove stopwords, punctuation and symbols
'''


def stop_word_removal(tweets):
    punctuation = list(string.punctuation)
    chars_to_remove = '[!@#$ .?,]'

    try:

        stop = stopwords.words('english') + punctuation + ['rt', 'via']

    except LookupError:
        stop = punctuation + ['rt', 'via']

    for tweet in tweets:
        tweet['terms'] = [re.sub(chars_to_remove, '', term).lower() for term in tweet['text'].split(' ') if
                          term not in stop]

    return tweets


'''
Count occurrences of each word
'''


def term_frequency(tweets):
    word_freq = count_terms.most_common_terms(tweets).most_common(30)
    labels, freq = zip(*word_freq)
    data = {'data': freq, 'x': labels}
    bar = vincent.Bar(data, iter_idx='x')
    bar.to_json('term_freq.json')


'''
For each word searched, get date when tweet is created
'''


def time_series(tweets):
    dates_created = dict()

    for tweet in tweets:

        if 'terms' in tweet:
            text = tweet['terms']
        else:
            text = tweet['text'].split(' ')

        if 'user' in tweet:
            if 'created_at' in tweet['user']:

                for term in twitter_search.to_search_for:
                    if term in text:
                        if term not in dates_created:
                            dates_created[term] = []

                        dates_created[term].append(tweet['user']['created_at'])

    series_created = {}
    per_business_day = {}

    # for every searched term
    for term in twitter_search.to_search_for:

        if term not in dates_created:
            continue

        # a list of ones to count
        ones = [1] * len(dates_created[term])
        # the index of the series
        idx = pandas.DatetimeIndex(dates_created[term])
        # the actual series (at series of 1s for the moment)
        series_created[term] = pandas.Series(ones, index=idx)

        # Bucketing
        per_business_day[term] = series_created[term].resample('B', how='sum').fillna(0)

    # and now the plotting
    time_chart = None

    for term in series_created:
        time_chart = vincent.Line(per_business_day[term])

        time_chart.axis_titles(x='Time', y='Freq')
        time_chart.legend(title='term')
        time_chart.to_json('time_chart_' + term + '.json')


'''
Get data about connections among users
'''


def get_user_connections(tweets):
    user_data = {
        "id": [],
        "name": [],
        "friends_count": [],
        "followers_count": [],
        "list_of_followers": []
    }

    for tweet in tweets:
        if "user" in tweet:
            if 'followers_count' and 'id_str' and 'friends_count' in tweet['user']:
                user_data['id'].append(tweet['user']['id_str'])
                user_data['name'].append(tweet['user']['name'])
                user_data['friends_count'].append(tweet['user']['friends_count'])
                user_data['followers_count'].append(tweet['user']['followers_count'])

    # for each of the users (id), find friends
    user_followers = twitter_search.fetch_followers_ids(user_data)

    user_followers_json = json.dumps(user_followers, encoding="utf-8", ensure_ascii=False)

    with open(followers_list_file_path, "w+"):
        pass

    # save to file
    with open(followers_list_file_path, "a+") as my_file:
        my_file.write(str(user_followers_json.encode('utf-8')))


'''
Graph of connections among users
'''


def plot_user_connections():
    try:
        # parse followers
        with open(followers_list_file_path) as file:
            user_followers = json.load(file, encoding='utf-8')

        if not user_followers:
            print("No data in user followers file.")

        edges = []

        #TODO plot connections among users

        '''
        for user in user_followers:

            list_of_followers = user_followers[user]
            # follower = id_str
            for follower in list_of_followers:
                edges.append([long(user), follower])
        '''

    except(OSError, IOError):
        print("[ERR] File not found at location: " + followers_list_file_path)
        sys.exit(1)


if __name__ == '__main__':
    # Score contains word - sentiment_score values
    scores = get_scores()

    # Full sample of tweets
    tweets = get_tweets()

    # Analyze sentiments
    analyze_tweets(tweets)

    # remove stop-words
    tweets = stop_word_removal(tweets) # in case of word frequency analysis

    # distribution of tweets over time
    time_series(tweets)

    # frequency visualization
    term_frequency(tweets)

    # Assign new sentiments
    assign_new_scores(tweets, scores)

    # Location (pos/neg)
    determine_location_of_tweets(tweets)

    '''
    # About connections
    get_user_connections(tweets)

    # user social graph
    plot_user_connections()
    '''

import oauth2 as oauth
import urllib2
import urllib
import json
import sys
import os
import time

'''
ENV:
Python 2.7

Ref:
https://dev.twitter.com/rest/reference/get/search/tweets
https://dev.twitter.com/rest/public/search

Desc:
Using Twitter API to fetch sample of tweets that contain words of interest set in setup region
'''

dir = os.path.dirname(__file__)

# ####################################################################
# Beginning of setup region
# ####################################################################

# To customize your twitter search, tweak following variables:

search_results_file_path = os.path.join(dir, 'search_results_1.json')

to_search_for = ['trading', 'stocks', 'forex', 'online trade', 'broker']

count = 100  # number of tweets per request, max = 100
max_tweets = 1000  # max = count * 450
language = "en" # language of tweets
since_id = None
max_id = None

# ####################################################################
# End of setup region
# ################################################################

api_key = "yCx3529Bt6Ji3vKdr8AmId7gv"
api_secret = "DJbZnXkoOSsx4A7PlvJwTlOiD5tiPkXi49yRdvKmnBsnmXkTL9"
access_token_key = "3865444774-3TU3zK5JUpUxUa1nSAv7a8tCuDUNSP96qVNjHhS"
access_token_secret = "QbDGY1BDS8qVcFEVAycM35WPQpdBP8UPRw5dUhPJqPEes"

_debug = 0

oauth_token = oauth.Token(key=access_token_key, secret=access_token_secret)
oauth_consumer = oauth.Consumer(key=api_key, secret=api_secret)

signature_method_hmac_sha1 = oauth.SignatureMethod_HMAC_SHA1()

http_method = "GET"

http_handler = urllib2.HTTPHandler(debuglevel=_debug)
https_handler = urllib2.HTTPSHandler(debuglevel=_debug)

search_api = "https://api.twitter.com/1.1/search/tweets.json"

search_followers = "https://api.twitter.com/1.1/followers/ids.json"

max_requests = 450


def fetch_followers_ids(user_data):

    counter = 0

    user_followers = {}

    while (True):
        try:

            been_there_done_that = []

            for id in user_data['id']:

                counter = counter + 1

                if id not in been_there_done_that:

                    url = search_followers + "?" + 'cursor=-1&user_id=' + str(id) + '&count=5000'

                    response = twitter_request(url, "GET", [])

                    data = json.load(response)

                    if 'errors' in data:
                        raise IOError(data['errors'][0]['message'])

                    if 'ids' in data:
                        user_followers[id] = []
                        user_followers[id] = data['ids']
                    else:
                        continue

                    been_there_done_that.append(id)

                else:

                    print('duplicate key ' + id)  # TODO ovo samo za "debug"

            return user_followers

        except IOError, e:
            print("[ERR]" + e.message)
            '''
            print('sleep')
            time.sleep(15 * 60 + 15)
            continue
            '''
            # else:

            print('break')
            print(str(counter))
            return user_followers



def fetch_samples():
    try:

        query = " OR ".join(to_search_for)

        # delete file content
        with open(search_results_file_path, "w+"):
            pass

        url = build_url(query, language, count, since_id, max_id)

        tweet_count = 0
        tweets = []

        while tweet_count < max_tweets:

            # request tweets
            response = twitter_request(url, "GET", [])

            # response to json
            data = json.load(response)

            # e.g. too many requests
            if "errors" in data:
                print(data["errors"][0]["message"])
                break

            statuses = data["statuses"]

            for status in statuses:
                tweets.append(status)

            search_metadata = data["search_metadata"]
            '''
            for tweet in tweets:
                # Append tweets to a file
                with open(file_path, "a+") as my_file:
                    # json_tweet = json.dumps(tweet, encoding='utf-8',  ensure_ascii=False)
                    my_file.write(str(tweet.encode('utf-8')))
            '''

            tweet_count = len(tweets)
            print("Downloaded {0} tweets so far".format(tweet_count))

            if "next_results" in search_metadata:
                url = search_api + search_metadata["next_results"]  # + "&since_id=" + str(since_id)

            elif "max_id_str" in search_metadata:  # Note: search_metadata["refresh_url"]
                max_id_str = search_metadata["max_id_str"]
                url = build_url(query, language, count, since_id, max_id_str)

            if not url:
                print("No more tweets found")
                break

            if "completed_in" in search_metadata:
                completed_in = search_metadata["completed_in"]
                print("Completed in {0} sec".format(completed_in))

        tweets_json = json.dumps(tweets, encoding="utf-8", ensure_ascii=False)

        with open(search_results_file_path, "a+") as my_file:
            my_file.write(str(tweets_json.encode('utf-8')))

        print("Function end")

    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
    except KeyError as e:
        print("Key error ({0})".format(e.message))
    except:
        print "Unexpected error:", sys.exc_info()[0]
        raise


'''
Construct, sign, and open a twitter request
using the hard-coded credentials above.
'''


def twitter_request(url, method, parameters):
    req = oauth.Request.from_consumer_and_token(oauth_consumer,
                                                token=oauth_token,
                                                http_method=http_method,
                                                http_url=url,
                                                parameters=parameters)

    req.sign_request(signature_method_hmac_sha1, oauth_consumer, oauth_token)

    headers = req.to_header()

    if http_method == "POST":
        encoded_post_data = req.to_postdata()
    else:
        encoded_post_data = None
        url = req.to_url()

    opener = urllib2.OpenerDirector()
    opener.add_handler(http_handler)
    opener.add_handler(https_handler)

    response = opener.open(url, encoded_post_data)

    return response


'''
Build string url
'''


def build_url(query, language, count, since_id, max_id):
    d = {}

    d["include_entities"] = True

    d["q"] = query
    d["lang"] = language
    d["count"] = count

    if not since_id is None:
        d["since_id"] = since_id

    if not max_id is None:
        d["max_id"] = max_id

    query_string = urllib.urlencode(d)

    url = search_api + "?" + query_string

    return url


if __name__ == '__main__':

    # Check global vars
    if count > 100:
        count = 100

    if max_tweets > count * max_requests:
        max_tweets = count * max_requests

    # Perform function
    fetch_samples()

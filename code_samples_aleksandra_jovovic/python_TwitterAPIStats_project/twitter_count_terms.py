__author__ = 'Aleksandra'
from collections import Counter
import twitter_search
import re

chars_to_remove = '[!@#$ .]'
filter_words = twitter_search.to_search_for
filter_words = filter_words + (['RT', 'trade', '#trade', '#forex', '', '#rt', 'rt', '#fx', '#forexnews', '#news', 'free'])

def most_common_text(tweets):

    count_all_text = Counter()
    for tweet in tweets:
        # Create a list with all the terms
        terms_all = [term for term in tweet['text'].split(" ") if re.sub(chars_to_remove, '', term).lower() not in filter_words]
        # Update the counter
        count_all_text.update(terms_all)
    # Print the first 5 most frequent words
    print(count_all_text.most_common(10))

    return count_all_text

def most_common_terms(tweets):

    count_all_terms = Counter()
    for tweet in tweets:
        # Create a list with all the terms
        if "terms" in tweet:
            terms_all = [term for term in tweet['terms'] if re.sub(chars_to_remove, '', term).lower() not in filter_words]
        else:
            terms_all = [term for term in tweet['text'].split(" ") if re.sub(chars_to_remove, '', term).lower() not in filter_words]
        # Update the counter
        count_all_terms.update(terms_all)
    # Print the first 5 most frequent words
    print(count_all_terms.most_common(10))

    return count_all_terms

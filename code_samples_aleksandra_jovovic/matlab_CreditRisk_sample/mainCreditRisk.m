function creditRiskMain()
%CREDITRISKMAIN Summary of this function goes here
try
    
    clc;
    
    current_breakpoints = dbstatus('-completenames');
    clearvars -except current_breakpoints
    evalin('base', 'clear all');
    dbstop(current_breakpoints);
    pause(0.1);
    clear current_breakpoints
    
%     close all;
    
    % P je relativna putanja do foldera u kome se nalazi m. fajl
    P = mfilename('fullpath');
    K = strfind(P, '\');
    P(K(end):end) = '';
    
    
    %% Estimate threshoolds for each rating
    transitionMatrix = xlsread([P '\CMTransMatrix.xlsx'], 'B2:I8'); % probabilities(transition matrix)
    
    numberOfThresholds = length(transitionMatrix)-1; % without threshold for Aaa rating
    thresholds = zeros(numberOfThresholds, numberOfThresholds); % preallocation
    
    for i = 1:numberOfThresholds
        for j = length(transitionMatrix):-1:2
            thresholds(j-1, i) = norminv(sum(transitionMatrix(i, j:length(transitionMatrix))/100));
        end
    end
    disp('---Thresholds:')
    disp(thresholds);
    
    
    %% Assign thresholds to obligors (based on obligor rating)
    
    % Import dataset
    [~, ~, obliogorsIndicesMapping] = xlsread([P '\obligorsIndicesMapping.xlsx'], 'A2:M77');
    
    obligorCountry = obliogorsIndicesMapping(:, 1);
    obligorName = obliogorsIndicesMapping(:, 2);
    creditRating = obliogorsIndicesMapping(:, 3);
    specificVolatility = obliogorsIndicesMapping(:, 4);
    index = obliogorsIndicesMapping(:, 5);
    
    currency = obliogorsIndicesMapping(:, 8);
    maturity = obliogorsIndicesMapping(:, 9);
    countryBps = obliogorsIndicesMapping(:, 10);
    obligorBps = obliogorsIndicesMapping(:, 11);
    seniorityClass = obliogorsIndicesMapping(:, 12);
    principal = obliogorsIndicesMapping(:, 13);
    
    fieldNames = {...
        'country',...
        'obligorName',...
        'creditRating',...
        'specificVolatility',...
        'index',...
        'currency',...
        'maturity',...
        'countryBps',...
        'obligorBps',...
        'seniorityClass',...
        'principal'...
        };
    
    emptyCells = repmat(cell(1), 1, numel(fieldNames));
    entries = {fieldNames{:} ; emptyCells{:}};
    obligors = struct(entries{:});
    
    
    if ~isempty(obliogorsIndicesMapping)
        for i = 1:length(obliogorsIndicesMapping)
            obligors(i).country             = obligorCountry(i);
            obligors(i).obligorName         = obligorName{i};
            obligors(i).creditRating        = creditRating(i);
            obligors(i).specificVolatility  = specificVolatility{i};
            obligors(i).index               = index{i};
            obligors(i).currency            = currency{i};
            obligors(i).maturity            = maturity{i};
            obligors(i).countryBps          = countryBps{i};
            obligors(i).obligorBps          = obligorBps{i};
            obligors(i).seniorityClass      = seniorityClass{i};
            obligors(i).principal           = principal{i};
            
        end
    end
    
    % Assign thresholds to ratings
    numberOfObligors = length(obligors);
    obligorThresholdMatrix = zeros(numberOfThresholds, numberOfObligors);
    
    for i = 1:numberOfObligors
        if(strcmp(obligors(i).creditRating, 'AAA'))
            obligorThresholdMatrix(:, i) = thresholds(:, 1);
        elseif(strcmp(obligors(i).creditRating, 'AA'))
            obligorThresholdMatrix(:, i) = thresholds(:, 2);
        elseif(strcmp(obligors(i).creditRating, 'A'))
            obligorThresholdMatrix(:, i) = thresholds(:, 3);
        elseif(strcmp(obligors(i).creditRating, 'BBB'))
            obligorThresholdMatrix(:, i) = thresholds(:, 4);
        elseif(strcmp(obligors(i).creditRating, 'BB'))
            obligorThresholdMatrix(:, i) = thresholds(:, 5);
        elseif(strcmp(obligors(i).creditRating, 'B'))
            obligorThresholdMatrix(:, i) = thresholds(:, 6);
        elseif(strcmp(obligors(i).creditRating, 'CCC'))
            obligorThresholdMatrix(:, i) = thresholds(:, 7); % everything below this threshold is default state
        end
    end
    
    disp('')
    disp('---Threshold matrix of all obligors:')
    disp(obligorThresholdMatrix)
    
    %% Correlation between indices
    
    % ...hm...better way...???
    indices.latinAmerica    = xlsread([P '\YahooHistoricalData\iShares Latin America 40 (ILF).xlsx'],        'C3:C1220');
    indices.korea           = xlsread([P '\YahooHistoricalData\iShares MSCI South Korea Capped (EWY).xlsx'], 'C3:C1220');
    indices.banking         = xlsread([P '\YahooHistoricalData\SPDR S&P Regional Banking ETF (KRE).xlsx'],   'C3:C1220');
    
    indices.indonesia       = xlsread([P '\YahooHistoricalData\Vector Indices Indonesia (IDX).xlsx'],        'C3:C1220');
    indices.europe          = xlsread([P '\YahooHistoricalData\iShares Europe (IEV).xlsx'],                  'C3:C1220');
    indices.malaysia        = xlsread([P '\YahooHistoricalData\iShares MSCI Malaysia (EWM).xlsx'],           'C3:C1220');
    
    indices.mexico          = xlsread([P '\YahooHistoricalData\iShares MSCI Mexico Capped (EWW).xlsx'],      'C3:C1220');
    indices.mexicoIron      = xlsread([P '\YahooHistoricalData\Mexico Fund, Inc. (The) Common (MXF) - Iron ore.xlsx'],          'C3:C1220');
    indices.thailand        = xlsread([P '\YahooHistoricalData\iShares MSCI Thailand Capped (THD).xlsx'],    'C3:C1220');
    
    numberOfIndices = length(fieldnames(indices));
    
    % Mean, average daily return for index
    meanLatinAmerica    = mean(indices.latinAmerica);
    meanKorea           = mean(indices.korea);
    meanBanking         = mean(indices.banking);
    meanIndonesia       = mean(indices.indonesia);
    meanEurope = mean(indices.europe);
    meanMalaysia = mean(indices.malaysia);
    meanMexico = mean(indices.mexico);
    meanMexicoIron = mean(indices.mexicoIron);
    meanThiland = mean(indices.thailand);
    
    % Standard Deviation, deviation from the mean
    stdLatinAmerica     = std(indices.latinAmerica);
    stdKorea            = std(indices.korea);
    stdBanking          = std(indices.banking);
    
    stdIndonesia       = std(indices.indonesia);
    stdEurope = std(indices.europe);
    stdMalaysia = std(indices.malaysia);
    stdMexico = std(indices.mexico);
    stdMexicoIron = std(indices.mexicoIron);
    stdThiland = std(indices.thailand);
    
    % Standardize indices returns
    indices.latinAmerica    = (indices.latinAmerica-meanLatinAmerica)/stdLatinAmerica;
    indices.korea           = (indices.korea-meanKorea)/stdKorea;
    indices.banking         = (indices.banking-meanBanking)/stdBanking;
    
    indices.indonesia       = (indices.indonesia-meanIndonesia)/stdIndonesia;
    indices.europe          = (indices.europe-meanEurope)/stdEurope;
    indices.malaysia        = (indices.malaysia-meanMalaysia)/stdMalaysia;
    
    indices.mexico          = (indices.mexico-meanMexico)/stdMexico;
    indices.mexicoIron      = (indices.mexicoIron-meanMexicoIron)/stdMexicoIron;
    indices.thailand        = (indices.thailand-meanThiland)/stdThiland;
        
    
    % correlation matrix
    indicesCorrelationMatrix = corrcoef([...
                                            indices.latinAmerica,   indices.korea,      indices.banking, ...
                                            indices.indonesia,      indices.europe,     indices.malaysia, ...
                                            indices.mexico,         indices.mexicoIron, indices.thailand ...
                                            ]); % C_k
    
    figure;
    heatmap(indicesCorrelationMatrix, fieldnames(indices),  fieldnames(indices), '%0.4f', 'TickAngle', 45,'ShowAllTicks', true, 'ColorMap', 'cool');
    title('Indices Correlation Matrix');
    
    
    %% Correlation matrix between Obligors
    
    % n = numberOfObligors
    % m = numberOfIndices
    
    downRight = eye(numberOfObligors);
    downLeft = zeros(numberOfObligors, numberOfIndices);
    
    upRight = zeros(numberOfIndices, numberOfObligors);
    upLeft = indicesCorrelationMatrix;
    
    left = vertcat(upLeft, downLeft);
    right = vertcat(upRight, downRight);
    
    correlationMatrix_hat = horzcat(left, right); % correlationMatrix = weightMatrix' * correlationMatrix_hat * weightMatrix;
    
    %% Weight matrix
    
    upperWeightMatrix = zeros(numberOfIndices, numberOfObligors);
    
    % Weight w1 - systematic, index-related volatility
    % Weight w2 - idiosyncratic, firm-specific volatility
    
    w1 = [obligors(:).specificVolatility]'./100; % 1 - [obligors(:).specificVolatility]'./100;
    
    w2 = sqrt(1 - w1 .^ 2);
    
    for i = 1:numberOfIndices
        
        temp = fieldnames(indices);
        currentIndexName = temp{i};
        for j = 1:numberOfObligors
            if(strcmp(obligors(j).index, 'EMF Latin America') && strcmp(currentIndexName,'latinAmerica'))
                upperWeightMatrix(i, j) = w2(j); % inputWeights(1, j);
            elseif(strcmp(obligors(j).index, 'Banking') && strcmp(currentIndexName,'banking'))
                upperWeightMatrix(i, j) = w2(j);
            elseif(strcmp(obligors(j).index, 'Korea General') && strcmp(currentIndexName,'korea'))
                upperWeightMatrix(i, j) = w2(j);
            elseif(strcmp(obligors(j).index, 'Indonesia General') && strcmp(currentIndexName,'indonesia'))
                upperWeightMatrix(i, j) = w2(j);
            elseif(strcmp(obligors(j).index, 'Europe 14') && strcmp(currentIndexName,'europe'))
                upperWeightMatrix(i, j) = w2(j);
            elseif(strcmp(obligors(j).index, 'Malaysia General') && strcmp(currentIndexName,'malaysia'))
                upperWeightMatrix(i, j) = w2(j);
            elseif(strcmp(obligors(j).index, 'Mexico Metals Mining') && strcmp(currentIndexName,'mexicoIron'))
                upperWeightMatrix(i, j) = w2(j);
            elseif(strcmp(obligors(j).index, 'Mexico General') && strcmp(currentIndexName,'mexico'))
                upperWeightMatrix(i, j) = w2(j);
            elseif(strcmp(obligors(j).index, 'Thailand Banking') && strcmp(currentIndexName,'thailand'))
                upperWeightMatrix(i, j) = w2(j);
                
            else
                % disp(['There is no suitable index for obligor ' obligors(j).obligorName]);
            end
            
        end
        
    end
    
    lowerWeightMatrix = diag(w1);
    
    weightMatrix = vertcat(upperWeightMatrix, lowerWeightMatrix); % W
    
    %% Correlation matrix
    
    correlationMatrix = weightMatrix' * correlationMatrix_hat * weightMatrix; % Matrix giving the correlation between all of the obligors
    
    figure;
    heatmap(correlationMatrix, {obligors.obligorName},  {obligors.obligorName}, '%0.2f', 'TickAngle', 45,'ShowAllTicks', true, 'ColorMap', 'cool');
    title('Obligors Correlation Matrix')
    
    %% Simmulate scenarios using Gaussian Copula
    
    numberOfSimulations = 2e4;
    
    choleskyFactor = chol(correlationMatrix, 'lower'); % Cholesky factor of correlation matrix
    rng shuffle
%     Z = randn(numberOfSimulations, numberOfObligors); % matrix of pseudorandom values drawn from the standard normal distribution.
%     U = zeros(numberOfSimulations, numberOfObligors); % preallocation
    
    U = randn(numberOfSimulations, numberOfObligors) * choleskyFactor';

    
    %% Mapping simulated asset returns (compare to thresholds)
    % Assign new rating to every generated scenario
    
    dataForVisualisation = zeros(8, numberOfObligors);
    
    for i1 = 1:length(obligors)
        
        obligors(i1).newRating.simulation = cell(numberOfSimulations, 1);
        
    end
    
    
    for j = 1:numberOfObligors
        
        for i = 1:numberOfSimulations
            
            if U(i,j) < obligorThresholdMatrix(7,j) % below 7th threshold - default, for j-th obligor
                obligors(j).newRating.simulation(i) = {'Default'};
                dataForVisualisation(8, j) = dataForVisualisation(8, j) + 1;
                
            elseif U(i,j) > obligorThresholdMatrix(7,j) && U(i,j) < obligorThresholdMatrix(6,j)
                obligors(j).newRating.simulation(i) = {'CCC'};
                dataForVisualisation(7, j) = dataForVisualisation(7, j) + 1;
                
            elseif U(i,j) > obligorThresholdMatrix(6,j) && U(i,j) < obligorThresholdMatrix(5,j)
                obligors(j).newRating.simulation(i) = {'BBB'};
                dataForVisualisation(6, j) = dataForVisualisation(6, j) + 1;
                
            elseif U(i,j) > obligorThresholdMatrix(5,j) && U(i,j) < obligorThresholdMatrix(4,j)
                obligors(j).newRating.simulation(i) = {'BB'};
                dataForVisualisation(5, j) = dataForVisualisation(5, j) + 1;
                
            elseif U(i,j) > obligorThresholdMatrix(4,j) && U(i,j) < obligorThresholdMatrix(3,j)
                obligors(j).newRating.simulation(i) = {'B'};
                dataForVisualisation(4, j) = dataForVisualisation(4, j) + 1;
                
            elseif U(i,j) > obligorThresholdMatrix(3,j) && U(i,j) < obligorThresholdMatrix(2,j)
                obligors(j).newRating.simulation(i) = {'A'};
                dataForVisualisation(3, j) = dataForVisualisation(3, j) + 1;
                
            elseif U(i,j) > obligorThresholdMatrix(2,j) && U(i,j) < obligorThresholdMatrix(1,j)
                obligors(j).newRating.simulation(i) = {'AA'};
                dataForVisualisation(2, j) = dataForVisualisation(2, j) + 1;
                
            elseif U(i,j) > obligorThresholdMatrix(1,j)
                obligors(j).newRating.simulation(i) = {'AAA'};
                dataForVisualisation(1, j) = dataForVisualisation(1, j) + 1;
                
            end
            
        end
        
    end
    
    clear U newCRMatrix
    
    % Data visualization, example
    obligorNumber = 34;
    data = dataForVisualisation(:, obligorNumber)'; % eg. Obligor 34, CCC
    figure;
    rng(0,'twister')
    bar(data);
    h = findobj(gca,'Type','patch');
    set(h,'FaceColor',[0 .5 .5],'EdgeColor','w');
    set(gca,'XTickLabel',{'AAA', 'AA', 'A', 'B', 'BB', 'BBB', 'CCC', 'Default'})
    title('10000 simulated scenarios for Obligor #34');
    
    %% %%%%%%%%%%%%%%%%%%
    %  Valuation
    %%%%%%%%%%%%%%%%%%%%%
    
    %% Country Risk Premium
    
	% [Here goes the code]
	
    
    %% Default spreads for each credit rating
    
	% [Here goes the code]
	
    
    %% Interest rates
	
	% [Here goes the code]
	
    
    % Assign interest rates to obligors
	
	% [Here goes the code]
	
    
    %% Zero curves
    % here: EURIBOR + Country BPs
    
	% [Here goes the code]
    
    
    %% 1.1 Valuation - In case of Default
    
    % Estimate beta params using mean and standard deviation
	
	% [Here goes the code]
	

    % Assign recovery rate based on simulated RR derived from Beta dist.
    
	% [Here goes the code]
	
	
    %% 1.2 Valuation - In case of up/down movements
    
    % Payoff for every obligor (CF based on rating) for every simulation
    % equals interest rate increased by the obligor's risk premium
    % here: CF = EURIBOR(12 months) + Country BPs + Obligor BPs + Bank premium

	% [Here goes the code]
	
    
catch ex
    rethrow(ex);
end

end

